package com.example.vinita.searchonfb2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        ImageView imageView = (ImageView)findViewById(R.id.imageView2);
        TextView textView = (TextView)findViewById(R.id.textView3);

        textView.setText(getIntent().getStringExtra("Country"));
        imageView.setImageResource(getIntent().getIntExtra("Flag",R.drawable.com_facebook_favicon_blue));
    }
}
