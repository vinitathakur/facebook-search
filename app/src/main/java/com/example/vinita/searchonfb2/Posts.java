package com.example.vinita.searchonfb2;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;


/**
 * A simple {@link Fragment} subclass.
 */
public class Posts extends Fragment {

    JSONArray posts=null;
    String created_time[]=null;
    String message[] = null;
    String id;
    String name;


    public Posts() {
        // Required empty public constructor
    }

    public Posts(JSONArray posts, String id, String name) throws JSONException {
        if(posts!=null)
        {
            this.id = id;
            this.name = name;
            this.posts = posts;
            created_time = new String [posts.length()];
            message = new String[posts.length()];

            for (int m=0;m<posts.length();m++)
            {
                String created_time1 = posts.getJSONObject(m).getString("created_time");
                String message1 = posts.getJSONObject(m).getString("message");
                created_time[m] = created_time1;
                message[m]=message1;

            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_posts, container, false);
        ListView listView = (ListView)view.findViewById(R.id.listViewPosts);

        TextView tv_post = (TextView)view.findViewById(R.id.tv_post);
        if(posts==null)
        {
            tv_post.setText("No posts available to display");
        }
        else
        {
            CustomAdapter customAdapter = new CustomAdapter();
            listView.setAdapter(customAdapter);
        }
        return view;



    }

    class CustomAdapter extends BaseAdapter
    {

        @Override
        public int getCount() {
            return created_time.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {

            view = getLayoutInflater(Bundle.EMPTY).inflate(R.layout.list_item_posts,null);
            TextView dandt = (TextView)view.findViewById(R.id.date);
            TextView msg = (TextView)view.findViewById(R.id.postdata);
            TextView profile_name = (TextView)view.findViewById(R.id.profile_name);

            ImageView pic = (ImageView)view.findViewById(R.id.pp);

            String url = "https://graph.facebook.com/v2.8/"+id+"/picture?access_token=EAADGtHaL6CABAE8lJ5Y7acUbxOY0FQIfkZANVzgeovQr2mwU5ZBlusWtb9SxGSEkez20Sz9qwF5tfCrLbV2KCLfATZCq6USYZAKvAFu89vHrImWR20c7dLxdG1bLuWih9M1JUdPdXrNgJBRGZCWFsRvLtMSh7FqIZD";
            profile_name.setText(name);

            String fp = created_time[position].substring(0,10);
            String lp = created_time[position].substring(11,19);

            dandt.setText(fp+" "+lp);
            msg.setText(message[position]);

            Picasso.with(getContext()).load(url).into(pic);


            return view;
        }
    }

}
