package com.example.vinita.searchonfb2;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MoreDetails extends AppCompatActivity {


    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    Intent here;
    String id;
    String name;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_more_details);
        setTitle("More Details");

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

        }

        here = getIntent();
        id = here.getStringExtra("id");
        name = here.getStringExtra("name");
        new MyTask().execute(id);


    }


    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        switch (item.getItemId()) {
            case R.id.share:
                share_on_fb();
                return true;
            case R.id.add_to_favorites:
                add_to_favorites();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void share_on_fb()
    {
        String imageurl = "https://graph.facebook.com/v2.8/"+id+"/picture?width=800&height=800";

        if (ShareDialog.canShow(ShareLinkContent.class)){
            ShareDialog shareDialog = new ShareDialog(this);
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle(name)
                    .setContentDescription("FB SEARCH FROM USC CSCI571")
                    .setContentUrl(Uri.parse("https://developers.facebook.com"))
                    .setImageUrl(Uri.parse(imageurl))
                    .build();

            shareDialog.show(linkContent);  // Show facebook ShareDialog<br />
        }
    }

    private void add_to_favorites()
    {

    }

    private class MyTask extends AsyncTask<String, Integer, String> {

        TextView temptv;

        // Runs in UI before background thread is called
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        // This is run in a background thread
        @Override
        protected String doInBackground(String... params) {

            String id = params[0];

            //build the URL
            String url = "http://localhost:81/logic.php?id="+id;

            //call the PHP file on AWS
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try
            {
                URL ur = new URL(url);
                connection = (HttpURLConnection) ur.openConnection();
                connection.connect();
                InputStream in = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(in));

                StringBuffer buffer = new StringBuffer();

                String line ="";
                while((line = reader.readLine())!=null){
                    buffer.append(line);
                }
                String finalJSON =  buffer.toString();
                return finalJSON;

            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            finally {
                if(connection!=null)
                {
                    connection.disconnect();
                }
                if(reader!=null)
                {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }


            for (int i = 0; i <= 100; i++) {
                publishProgress(i);
            }

            return null;
        }

        // This is called from background thread but runs in UI
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            // Do things like update the progress bar
        }

        // This runs in UI when background thread finishes
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            JSONObject parent = null;
            JSONArray albums = null;
            JSONArray posts = null;
            try {
                if(result!=null) {
                    parent = new JSONObject(result);

                    if (parent.getJSONObject("albums") != null) {
                        albums = parent.getJSONObject("albums").getJSONArray("data");
                    }
                    if (parent.getJSONObject("posts") != null) {
                        posts = parent.getJSONObject("posts").getJSONArray("data");
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            tabLayout = (TabLayout)findViewById(R.id.tabs2);
            viewPager = (ViewPager)findViewById(R.id.container2);

            viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

            viewPagerAdapter.addFragments(new Albums(albums),"Albums");
            try {
                viewPagerAdapter.addFragments(new Posts(posts,id,name),"Posts");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            viewPager.setAdapter(viewPagerAdapter);
            tabLayout.setupWithViewPager(viewPager);

        }
    }
}
