package com.example.vinita.searchonfb2;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ResultsPage extends AppCompatActivity {

    TextView tp;
    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results_page);

        JSONObject parentObject = null;
        JSONObject userObject = null;
        JSONArray userArray = null;

        JSONObject pageObject = null;
        JSONArray pageArray = null;

        JSONObject eventObject = null;
        JSONArray eventArray = null;

        JSONObject placeObject = null;
        JSONArray placeArray = null;

        JSONObject groupObject = null;
        JSONArray groupArray = null;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("Results");

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        // do the data fetching here
        //----------------------------------------------------------------------------------------------------------
        Intent i = getIntent();
        String json_data = i.getStringExtra("json_string_data");


        try {
            parentObject = new JSONObject(json_data);
            Log.d("data", String.valueOf(parentObject));

            userObject = parentObject.getJSONObject("user");
            userArray = userObject.getJSONArray("data");

            pageObject = parentObject.getJSONObject("page");
            pageArray = pageObject.getJSONArray("data");

            eventObject = parentObject.getJSONObject("event");
            eventArray = eventObject.getJSONArray("data");

            placeObject = parentObject.getJSONObject("place");
            placeArray = placeObject.getJSONArray("data");

            groupObject = parentObject.getJSONObject("group");
            groupArray = groupObject.getJSONArray("data");
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        //----------------------------------------------------------------------------------------------------------

        tabLayout = (TabLayout)findViewById(R.id.tabs);
        viewPager = (ViewPager)findViewById(R.id.container);

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        viewPagerAdapter.addFragments(new UserFragment(userArray),"Users");
        viewPagerAdapter.addFragments(new PageFragment(pageArray),"Pages");
        viewPagerAdapter.addFragments(new EventFragment(eventArray),"Events");
        viewPagerAdapter.addFragments(new PlacesFragment(placeArray),"Places");
        viewPagerAdapter.addFragments(new GroupsFragment(groupArray),"Groups");
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setIcon(R.mipmap.usericon);
        tabLayout.getTabAt(1).setIcon(R.mipmap.pageicon);
        tabLayout.getTabAt(2).setIcon(R.mipmap.eventicon);
        tabLayout.getTabAt(3).setIcon(R.mipmap.placeicon);
        tabLayout.getTabAt(4).setIcon(R.mipmap.groupicon);

        tabLayout.getTabAt(0).setText(viewPagerAdapter.getPageTitle(0));
        tabLayout.getTabAt(1).setText(viewPagerAdapter.getPageTitle(1));
        tabLayout.getTabAt(2).setText(viewPagerAdapter.getPageTitle(2));
        tabLayout.getTabAt(3).setText(viewPagerAdapter.getPageTitle(3));
        tabLayout.getTabAt(4).setText(viewPagerAdapter.getPageTitle(4));




    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

          }

