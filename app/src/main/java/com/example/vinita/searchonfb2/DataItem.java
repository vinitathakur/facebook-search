package com.example.vinita.searchonfb2;

/**
 * Created by Vinita on 4/26/2017.
 */

public class DataItem {

    int resIdThumbnail;
    String countryName;

    public DataItem(int resIdThumbnail, String countryName) {
        this.resIdThumbnail = resIdThumbnail;
        this.countryName = countryName;
    }
}
