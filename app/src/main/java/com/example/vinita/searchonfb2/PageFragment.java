package com.example.vinita.searchonfb2;


import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;


public class PageFragment extends Fragment {

    String[] IMAGES;
    String[] names;
    String[] ids;
    JSONArray page_json_data;

    public PageFragment() {
        // Required empty public constructor
    }

    public PageFragment(JSONArray page)
    {
        page_json_data = page;
        JSONObject inter = null;
        IMAGES = new String[page_json_data.length()];
        names = new String[page_json_data.length()];
        ids = new String[page_json_data.length()];
        for (int i = 0; i < page_json_data.length(); i++) {
            try {
                inter = page_json_data.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String name = null;
            String profile_picture = null;
            String id = null;
            URL url = null;
            Bitmap bmp = null;
            try {
                profile_picture = inter.getJSONObject("picture").getJSONObject("data").getString("url");
                name = inter.getString("name");
                id = inter.getString("id");
                ids[i]=id;
                IMAGES[i]=profile_picture;
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d("name", name);
            names[i] = name;

        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View inflatedView = inflater.inflate(R.layout.fragment_page, container, false);

        ListView listView = (ListView) inflatedView.findViewById(R.id.listViewPage);

        CustomAdapter customAdapter = new CustomAdapter();
        listView.setAdapter(customAdapter);

        return inflatedView;

    }

    class CustomAdapter extends BaseAdapter
    {

        @Override
        public int getCount() {
            return names.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {

            view = getLayoutInflater(Bundle.EMPTY).inflate(R.layout.customlayout,null);
            ImageView imageView = (ImageView)view.findViewById(R.id.imageView3);
            TextView textView_name = (TextView)view.findViewById(R.id.textView_name);
            ImageView detailinrow = (ImageView)view.findViewById(R.id.imageView7);

            Picasso.with(getContext()).load(IMAGES[position]).into(imageView);
            textView_name.setText(names[position]);

            AppendTags a = new AppendTags(ids[position],names[position]);
            detailinrow.setTag(a);

            detailinrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppendTags b = (AppendTags) v.getTag();
                    Intent i = new Intent(getActivity(),MoreDetails.class);
                    i.putExtra("id",b.id);
                    i.putExtra("name",b.name);
                    startActivity(i);

                };
            });

            return view;
        }
    }

}
