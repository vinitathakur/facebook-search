package com.example.vinita.searchonfb2;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.LocalSocketAddress;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static com.example.vinita.searchonfb2.R.id.container;
import static com.example.vinita.searchonfb2.R.id.image;
import static com.example.vinita.searchonfb2.R.id.imageView;

public class UserFragment extends Fragment{


    String[] IMAGES;
    String[] names;
    String[] ids;
    JSONArray user_json_data;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public UserFragment() {

    }

    public UserFragment(JSONArray user) {
        user_json_data = user;
        JSONObject inter = null;
        IMAGES = new String[user_json_data.length()];
        names = new String[user_json_data.length()];
        ids = new String[user_json_data.length()];
        for (int i = 0; i < user_json_data.length(); i++) {
            try {
                inter = user_json_data.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String name = null;
            String profile_picture = null;
            String id = null;
            URL url = null;
            Bitmap bmp = null;
            try {
                profile_picture = inter.getJSONObject("picture").getJSONObject("data").getString("url");
                name = inter.getString("name");
                id = inter.getString("id");
                ids[i] = id;
                IMAGES[i] = profile_picture;
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d("name", name);
            names[i] = name;

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View inflatedView = inflater.inflate(R.layout.fragment_user, container, false);

        ListView listView = (ListView) inflatedView.findViewById(R.id.listView);

        CustomAdapter customAdapter = new CustomAdapter();
        listView.setAdapter(customAdapter);

        return inflatedView;

    }


    class CustomAdapter extends BaseAdapter
    {

        @Override
        public int getCount() {
            return names.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {

            view = getLayoutInflater(Bundle.EMPTY).inflate(R.layout.customlayout,null);
            ImageView imageView = (ImageView)view.findViewById(R.id.imageView3);
            TextView textView_name = (TextView)view.findViewById(R.id.textView_name);
            ImageView detailinrow = (ImageView)view.findViewById(R.id.imageView7);

            Picasso.with(getContext()).load(IMAGES[position]).into(imageView);
            textView_name.setText(names[position]);

            AppendTags a = new AppendTags(ids[position],names[position]);
            detailinrow.setTag(a);


            detailinrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppendTags b = (AppendTags) v.getTag();
                    Intent i = new Intent(getActivity(),MoreDetails.class);
                    i.putExtra("id",b.id);
                    i.putExtra("name",b.name);
                    startActivity(i);

                };
            });

            return view;
        }
    }



}
