package com.example.vinita.searchonfb2;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Albums extends Fragment {


    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;


    JSONArray albums=null;
    public Albums() {
        // Required empty public constructor
    }

    public Albums(JSONArray albums)
    {
        if (albums!=null) {
            this.albums = albums;
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_albums, container, false);
        TextView tv_album = (TextView)view.findViewById(R.id.tv_album);
        if(albums==null)
        {
            tv_album.setText("No albums available to display");
        }
        else
        {
            expListView = (ExpandableListView)view.findViewById(R.id.lvExp);

            // preparing list data
            try {
                prepareListData();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            listAdapter = new ExpandableListAdapter(getActivity(), listDataHeader, listDataChild);

            // setting list adapter
            expListView.setAdapter(listAdapter);
        }
        return view;
    }

    private void prepareListData() throws JSONException {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();


        for (int f=0;f<albums.length();f++)
        {
            listDataHeader.add(albums.getJSONObject(f).getString("name"));
            JSONArray photodata = albums.getJSONObject(f).getJSONObject("photos").getJSONArray("data");
            List<String> temp = new ArrayList<String>();
            for(int x=0;x<photodata.length();x++)
            {
                temp.add(photodata.getJSONObject(x).getString("id"));
            }

            listDataChild.put(listDataHeader.get(f), temp);
        }
    }

}
