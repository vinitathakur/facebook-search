package com.example.vinita.searchonfb2;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    TextView enter_keyword;
    View navHeaderView;
    TextView fbsearchtv;
    Button search;
    Button clear;
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("Search on Facebook");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        enter_keyword = (TextView)findViewById(R.id.enter_keyword);
        enter_keyword.setTypeface(null, Typeface.BOLD);

        navHeaderView = navigationView.getHeaderView(0);
        fbsearchtv= (TextView) navHeaderView.findViewById(R.id.fbsearchtv);
        fbsearchtv.setTypeface(null,Typeface.BOLD);

        editText = (EditText)findViewById(R.id.editText);
        search = (Button)findViewById(R.id.search);
        clear = (Button)findViewById(R.id.clear);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int textlen = editText.getText().toString().trim().length();
                if(textlen==0)
                {
                    Toast.makeText(v.getContext(),"Please enter a keyword!",Toast.LENGTH_LONG).show();
                }
                else
                {
                    launchActivityResults();
                }
            }
        });

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText("");
            }
        });

    }


    private void launchActivityResults()
    {
        String keyword = editText.getText().toString();
        new MyTask().execute(keyword);
    }

    private void launchActivityAboutMe()
    {
        Intent intent = new Intent(this, AboutMe.class);
        startActivity(intent);
    }

    private void launchActivityFavorites()
    {
        Intent intent = new Intent(this, FavoritesActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {

        } else if (id == R.id.nav_fav) {
            launchActivityFavorites();
        }
        else if (id == R.id.nav_about) {
            launchActivityAboutMe();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private class MyTask extends AsyncTask<String, Integer, String> {

        // Runs in UI before background thread is called
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        // This is run in a background thread
        @Override
        protected String doInBackground(String... params) {

            String keyword = params[0];

            //build the URL
            String url = "http://localhost:81/logic.php?keyword="+keyword+"&latitude=34.032795400000005&longitude=-118.28188530000001";

            //call the PHP file on AWS
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try
            {
                URL ur = new URL(url);
                connection = (HttpURLConnection) ur.openConnection();
                connection.connect();
                InputStream in = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(in));

                StringBuffer buffer = new StringBuffer();

                String line ="";
                while((line = reader.readLine())!=null){
                    buffer.append(line);
                }
                String finalJSON =  buffer.toString();

                return finalJSON;

            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            finally {
                if(connection!=null)
                {
                    connection.disconnect();
                }
                if(reader!=null)
                {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            for (int i = 0; i <= 100; i++) {
                publishProgress(i);
            }

            return null;
        }

        // This is called from background thread but runs in UI
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            // Do things like update the progress bar
        }

        // This runs in UI when background thread finishes
        @Override
        protected void onPostExecute(String result) {
            if (result!=null)
            {
                super.onPostExecute(result);
                Intent intent = new Intent(getApplication(), ResultsPage.class);
                intent.putExtra("json_string_data", result);
                startActivity(intent);
            }
            else
            {
                System.out.println("Something is wrong with the PHP file hosting!");
            }
        }
    }

}
