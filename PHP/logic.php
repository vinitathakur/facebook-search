<?php


    require_once __DIR__ . '/php-graph-sdk-5.0.0/src/Facebook/autoload.php';
        $fb = new Facebook\Facebook([
          'app_id' => '<app-ID>',
          'app_secret' => '<app-secret>',
          'default_graph_version' => 'v2.8',
        ]);

        $fb->setDefaultAccessToken('<access-token>');

    if (isset($_GET["id"]))
    {
        $id = $_GET["id"];
        $detailsquery = $id."?fields=albums.limit(5){name,photos.limit(2){name,
picture}},posts.limit(5){created_time,message}";
        $detailsresponse = $fb->get($detailsquery);
        $get_details_data = $detailsresponse->getDecodedBody();
        echo json_encode($get_details_data);
    }
    else
    {
        $keyword = $_GET["keyword"];
        $latitude = $_GET["latitude"];
        $longitude = $_GET["longitude"];
        $query1="";
        $query2="";
        $query3="";
        $query4="";
        $query5="";

        $query1 = "search?q=".$keyword."&type=user&fields=id,name,picture.width(700).height(700)";
        $query2 = "search?q=".$keyword."&type=page&fields=id,name,picture.width(700).height(700)";
        $query3 = "search?q=".$keyword."&type=event&fields=id,name,picture.width(700).height(700)";
        $query4 = "search?q=".$keyword."&type=place&fields=id,name,picture.width(700).height(700)&center=".$latitude.",".$longitude;
        $query5 = "search?q=".$keyword."&type=group&fields=id,name,picture.width(700).height(700)";

        $response1 = $fb->get($query1);
        $response2 = $fb->get($query2);
        $response3 = $fb->get($query3);
        $response4 = $fb->get($query4);
        $response5 = $fb->get($query5);



        $get_data1 = $response1->getDecodedBody();
        $get_data2 = $response2->getDecodedBody();
        $get_data3 = $response3->getDecodedBody();
        $get_data4 = $response4->getDecodedBody();
        $get_data5 = $response5->getDecodedBody();


        echo json_encode(array('user'=>$get_data1,'page'=>$get_data2,'event'=>$get_data3,'place'=>$get_data4,'group'=>$get_data5));

    }
?>